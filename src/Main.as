/**
 * Created by dark256 on 20.06.17.
 */
package {

    import com.greensock.TweenLite;
    import com.greensock.easing.*;

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;

    [Frame(factoryClass="Preloader")]
    [SWF(width='310', height='450', backgroundColor='0x000000', frameRate='60')]

    public class Main extends Sprite {

        private var mainScreen:mainFon;
        private var aboutWin:AboutWindow;
        private var startButton:StartButton;
        private var virtualKeyboard:VirtualKeys;    //Клавиатура
        private var charTable:CharTable;            //Ячейки букв слова

        //В данном случае - генерится случайная точка входа в последовательность слов,
        //после чего далее слова выбираются по порядку
        private var wordIndex:int = Math.round( Math.random() * (ShareData.words.length - 1) );

        private var wordInGame:Array;               //Массив букв заданного слова
        private var wordStatus:Array;               //Признаки угаданных букв

        private var attempts:int;                   //Текущая попытка угадать букву
        private var wins:int = 0;                   //Кол-во побед
        private var lose:int = 0;                   //Кол-во поражений

        private var isAppRestarted:Boolean = false; //Флаг перезапуска

        public function Main() {
            trace( '■ Main' );
            if ( stage ) {
                initGame();
            } else {
                addEventListener( Event.ADDED_TO_STAGE, onAddToStage );
            }
        }

        private function onAddToStage( e:Event ):void {
            removeEventListener( Event.ADDED_TO_STAGE, onAddToStage );
            initGame();
        }

        /**
         * Установщик сцены с виселицей
         * @param stifPart - номер попытки.
         */
        private function set stifler( stifPart:Number ):void {
            if ( stifPart > 0 ) {
                mainScreen[ "s_" + stifPart ].visible = true;
            } else {
                for ( var i:int = 1; i < 8; i++ ) {
                    mainScreen[ "s_" + i ].visible = false;
                }
            }
        }

        private function initGame():void {
            trace( '∙ Main.initGame' );

            mainScreen                          = new mainFon();
            mainScreen.alpha                    = 0;
            mainScreen.hangMan.x                = -85;
            mainScreen.resaultText.visible      = false;
            mainScreen.resaultText.mouseEnabled = false;
            mainScreen.score.text               = '';
            stifler                             = 0;
            addChild( mainScreen );

            virtualKeyboard = new VirtualKeys( this, onKeyPressed );
            startButton     = new StartButton( mainScreen.buttonStart, onButtonStartClick );

            TweenLite.to( mainScreen, 1.5, {alpha: 1, onComplete: onBackVisible} );
        }

        // Бэкграунд прорисовался
        private function onBackVisible():void {
            aboutWin = new AboutWindow( this, waiting4Start );
            TweenLite.to( mainScreen.hangMan, 1.5, {x: 5, ease: Sine.easeOut} );    //Выводим палача из "кустов"
        }

        // Ждём старта-рестарта
        private function waiting4Start():void {
            startButton.show();
        }

        private function onButtonStartClick( event:MouseEvent ):void {
            SND.playSound( SND.FX_CLICK );
            startButton.hide( prepareStartGame );
        }

        private function prepareStartGame():void {
            if ( isAppRestarted ) {
                stifler                        = 0;
                mainScreen.resaultText.visible = false;
                charTable.clear( startGame );
            } else {
                startGame();
            }
        }

        private function startGame():void {
            if ( aboutWin ) {
                // Удаляем окно с описанием полностью, раз и навсегда, т.к. далее оно нам не нужно.
                aboutWin.close();
                aboutWin = null;
            }

            virtualKeyboard.show();
            wordInGame = ShareData.words[ wordIndex ].split( '' );
            charTable  = new CharTable( this, wordInGame );

            wordStatus = [];
            attempts  = 0;
        }

        //Обработчик нажатий виртуальных кнопок
        private function onKeyPressed( incomedCharacter:String ):void {

            var rez:Boolean = false;

            for ( var i:int = 0; i < wordInGame.length; i++ ) {
                //В данном случае повторное нажатие УГАДАННОЙ буквы считается ошибкой.
                if ( wordInGame[ i ] == incomedCharacter && wordStatus[ i ] != 1 ) {
                    wordStatus[ i ] = 1;
                    charTable.showCharAt( i );
                    rez = true;
                    SND.playSound( SND.FX_CLICK );
                }
            }

            if ( !rez ) {
                SND.playSound( SND.FX_WOOD );
                attempts++;
                stifler = attempts;
                if ( attempts > ShareData.maxAttempts ) {
                    gameOver( true );       //Попытки исчерпаны - гамовер есть гамовер!
                }
            } else {
                if ( checkWordStatus() ) {
                    gameOver( false );      //Буквы угаданы - жизнь продолжается :)
                }
            }
        }

        //Проверка - все ли буквы угаданы
        private function checkWordStatus():Boolean {
            var ct:int      = 0;
            var rez:Boolean = false;
            for ( var i:int = 0; i < wordInGame.length; i++ ) {
                if ( wordStatus[ i ] == 1 ) {
                    ct++
                }
            }
            if ( ct == wordInGame.length ) {
                rez = true;
            }
            return rez;
        }

        private function gameOver( status:Boolean ):void {
            virtualKeyboard.hide();
            if ( !status ) {
                SND.playSound( SND.FX_WIN );
                mainScreen.resaultText.visible = true;
                wins++;
            } else {
                SND.playSound( SND.FX_LOSE );
                lose++;
            }
            isAppRestarted = true;
            TweenLite.delayedCall( 2, showResaults );
            TweenLite.delayedCall( 3, waiting4Start );
        }

        private function showResaults():void {
            mainScreen.score.text = 'Выиграно: ' + wins + '  Проиграно: ' + lose;
            wordIndex++;
            if ( wordIndex >= ShareData.words.length ) {
                wordIndex = 0;
            }
        }
    }
}
