/**
 * Created by dark256 on 20.06.17.
 */
package {

    import com.greensock.TweenLite;
    import com.greensock.easing.Expo;

    import flash.display.DisplayObjectContainer;

    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.MouseEvent;

    public class VirtualKeys extends MovieClip {

        private const keyboardYPosition:int = 140;      //Позиция кнопок отн. нижнего края стэйджа

        private var doc:DisplayObjectContainer;
        private var vKeyboard:MovieClip = new MovieClip();
        private var callBack:Function;

        public function VirtualKeys( doc:DisplayObjectContainer, callBack:Function ) {
            trace( '∙ VirtualKeys' );
            this.callBack = callBack;
            this.doc = doc;

            var btn:MovieClip;
            var col:int = 0;
            var row:int = 0;
            for ( var i:int = 1040; i<1072; i++ ){
                btn = new keyBtnClip();
                btn.char.text = String.fromCharCode(i);
                btn.char.mouseEnabled = false;
                btn.x = col*26;
                btn.y = row*26;
                vKeyboard.addChild( btn );

                col++;
                if ( col>7 ){
                    col = 0;
                    row++;
                }
            }

            vKeyboard.addEventListener( MouseEvent.CLICK, keyBoardClick );
            vKeyboard.addEventListener( Event.ADDED_TO_STAGE, OnAddToStage );
            doc.addChild( vKeyboard );
            vKeyboard.mouseChildren = false;    // Блокируем клавиатуру
        }

        private function keyBoardClick( e:MouseEvent ):void{
            if ( vKeyboard.mouseChildren ) {
                callBack( e.target.parent.char.text );
            }
        }

        private function OnAddToStage( e:Event ):void {
            vKeyboard.removeEventListener( Event.ADDED_TO_STAGE, OnAddToStage );
            vKeyboard.x = 92;
            vKeyboard.y = vKeyboard.stage.stageHeight + 10;
        }

        public function show():void{
            vKeyboard.y = vKeyboard.stage.stageHeight + 10;
            vKeyboard.mouseChildren = true;
            TweenLite.to( vKeyboard, 0.7, { y: vKeyboard.stage.stageHeight - keyboardYPosition, ease: Expo.easeOut} );
        }

        public function hide():void{
            vKeyboard.mouseChildren = false;
            TweenLite.to( vKeyboard, 0.7, { y: vKeyboard.stage.stageHeight + 10, ease: Expo.easeIn} );
        }
    }
}
