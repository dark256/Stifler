/**
 * Created by dark256 on 20.06.17.
 */
package {

    import flash.media.Sound;
    import flash.media.SoundTransform;

    public class SND {

        // Звуки берутся из общей библиотеки
        // Не стал выносить в подгружаемые либы. Упаковал в прилинковываемую SWC вместе с графикой.
        public static const FX_WIN:Class   = win;
        public static const FX_LOSE:Class  = lose;
        public static const FX_CLICK:Class = click;
        public static const FX_WOOD:Class  = wood;
        public static const FX_CARD:Class  = card;

        public function SND() {
            trace( '∙ SND.SND' );
        }

        public static function playSound( className:Class ):void {
            var sound:Sound                    = new className();
            var volumeTransform:SoundTransform = new SoundTransform();
            volumeTransform.volume             = ShareData.Volume;
            sound.play( 0, 0, volumeTransform );
        }
    }
}
