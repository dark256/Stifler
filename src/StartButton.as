/**
 * Created by Root on 20.06.17.
 */
package {

    import com.greensock.TweenLite;
    import com.greensock.easing.Bounce;
    import com.greensock.easing.Sine;

    import flash.display.DisplayObject;
    import flash.events.MouseEvent;

    public class StartButton {

        private var startButton:DisplayObject;

        public function StartButton( btn:DisplayObject, callBack:Function ) {
            this.startButton = btn;
            startButton.x = 320;
            startButton.y = 330;
            startButton.addEventListener( MouseEvent.CLICK, callBack );
        }

        public function show():void{
            TweenLite.to( startButton, 0.5, {x: 98, ease: Bounce.easeOut} );
        }

        public function hide( callBack:Function ):void{
            TweenLite.to( startButton, 0.3, {x: 320, ease: Sine.easeIn, onComplete: callBack } );
        }
    }
}
