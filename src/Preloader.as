/**
 * Created by dark256 on 20.06.17.
 */
package {

    import flash.display.DisplayObject;
    import flash.display.MovieClip;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.ProgressEvent;
    import flash.utils.getDefinitionByName;

    public class Preloader extends MovieClip {

        private var loaderFon:preloaderFon;
        private var loaderBar:preloaderBar;

        public function Preloader() {

            trace( '- Preloader.start' );

            addEventListener( Event.ADDED_TO_STAGE, onAddToStage );

            loaderFon = new preloaderFon();
            addChild( loaderFon );

            loaderBar            = new preloaderBar();
            loaderBar.loads.text = "";
            loaderBar.maska.x    = loaderBar.progress.x - loaderBar.progress.width;
            addChild( loaderBar );

            addEventListener( Event.ENTER_FRAME, checkFrame );
            loaderInfo.addEventListener( ProgressEvent.PROGRESS, loadProgress );
            loaderInfo.addEventListener( IOErrorEvent.IO_ERROR, ioError );

        }


        private function onAddToStage( event:Event ):void {
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align     = StageAlign.TOP_LEFT;
            stage.addEventListener( Event.RESIZE, OnResize );
            OnResize( null );
            stage.removeEventListener( Event.ADDED_TO_STAGE, onAddToStage );

            if ( stage.loaderInfo.url.substring( 0, 6 ) != "file:/" ) {
                stage.showDefaultContextMenu = false;
            }
        }

        private function loadProgress( e:ProgressEvent ):void {
            var percents:Number  = (e.bytesLoaded / e.bytesTotal);
            loaderBar.maska.x    = loaderBar.progress.x - loaderBar.progress.width * (1 - percents);
            loaderBar.loads.text = Math.round( percents * 100 ) + "%";
        }

        private function checkFrame( e:Event ):void {
            if ( currentFrame == totalFrames ) {
                stop();
                appLoaded();
            }
        }

        private function ioError( e:IOErrorEvent ):void {
            trace( e.text );
        }

        private function OnResize( e:Event ):void {
            if ( stage ) {
                loaderBar.x = (stage.stageWidth - loaderBar.back.width) / 2;
                loaderBar.y = (stage.stageHeight - loaderBar.back.height) / 2 - 20;
            }
        }

        private function appLoaded():void {
            trace( '- Preloader.complete' );
            removeEventListener( Event.ENTER_FRAME, checkFrame );
            loaderInfo.removeEventListener( ProgressEvent.PROGRESS, loadProgress );
            loaderInfo.removeEventListener( IOErrorEvent.IO_ERROR, ioError );

            removeChild( loaderFon );
            removeChild( loaderBar );
            stage.removeEventListener( Event.RESIZE, OnResize );

            var mainClass:Class = getDefinitionByName( "Main" ) as Class;
            addChild( new mainClass() as DisplayObject );
        }
    }
}