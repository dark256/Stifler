/**
 * Created by dark256 on 20.06.17.
 */
package {

    import flash.display.DisplayObject;

    import avmplus.getQualifiedClassName;

    public class ShareData {

        public static const Volume:Number = 0.5;          //Громкость
        public static var _DEBUG:Boolean  = false;      //Флаг отладки
        public static var maxAttempts:int = 6;          //Макс кол-во попыток

        //Набор слов из Т.З.
        //Можно подгружать из какого-нть внешнего файла. В данном случае не стал заморачиваться.
        public static var words:Array = [ 'ОДИН', 'ДВА', 'ТРИ', 'ПЯТЬДЕСЯТ', 'ЛОПАТА' ];

        public function ShareData() {}

        public static function whereAmI( place:* ):void {
            trace( '---------where Am I start--------' );
            var muhParent:DisplayObject = place as DisplayObject;
            while (muhParent) {
                trace( '\t∙', muhParent, muhParent.name, getQualifiedClassName( muhParent ) );
                muhParent = muhParent.parent;
            }
            trace( '---------where Am I end--------' );
        }

//        public static function localCheck():Boolean {
//            return (stage.loaderInfo.url.substring( 0, 6 ) == "file:/" ? true : false);
//        }

    }
}