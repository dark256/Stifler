/**
 * Created by dark256 on 20.06.17.
 */
package {

    import com.greensock.TweenLite;
    import com.greensock.easing.Elastic;
    import com.greensock.easing.Sine;

    import flash.display.DisplayObjectContainer;

    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    public class CharTable extends MovieClip {

        private var doc:DisplayObjectContainer;
        private var callBack:Function;

        private var charTable:MovieClip = new MovieClip();
        private var charBoxes:Array     = [];
        private var boxWidth:int = 27;
        private var boxAmount:int;

        /**
         *
         * @param chars - массив букв слова
         * @param callBack - метод, вызываемый после очистки и удаления ячеек с текущими символами слова
         */
        public function CharTable( doc:DisplayObjectContainer, chars:Array ) {

            trace( '∙ CharTable.CREATE' );

            this.doc = doc;
            boxAmount = chars.length;

            var boxCount:int = 0;
            var box:charBox;
            var timerAct:Timer = new Timer( 100 );
            timerAct.addEventListener( TimerEvent.TIMER, OnTimer );
            timerAct.start();

            function OnTimer( e:TimerEvent ):void {
                if ( boxCount < boxAmount ){
                    SND.playSound( SND.FX_CARD );
                    box           = new charBox();
                    box.char.text = chars[ boxCount ];
                    if ( ShareData._DEBUG ) {
                        box.char.alpha = 0.4;
                    } else {
                        box.char.alpha = 0.0;
                    }
                    box.x = boxWidth * boxCount;
                    TweenLite.to( box, 1.2, {y: 40, ease: Elastic.easeOut} );
                    charBoxes.push( charTable.addChild( box ) );
                    boxCount++;
                } else {
                    timerAct.stop();
                    timerAct.removeEventListener( TimerEvent.TIMER, OnTimer );
                }
            }

            charTable.addEventListener( Event.ADDED_TO_STAGE, OnAddToStage );
            doc.addChild( charTable );
        }

        private function OnAddToStage( e:Event ):void {
            charTable.removeEventListener( Event.ADDED_TO_STAGE, OnAddToStage );
            charTable.x = Math.round( ( charTable.stage.stageWidth - boxWidth*boxAmount ) / 2 );
            charTable.y = 0;
        }

        //Показать угаданные буквы
        public function showCharAt( i:int ):void {
            charBoxes[ i ].char.alpha = 1;
        }

        public function clear( callBack:Function ):void {
            this.callBack = callBack;
            TweenLite.to( charTable, 0.7, {y: -100, ease: Sine.easeIn, onComplete: clearComplete} );
        }

        private function clearComplete():void {
            for ( var i:int = 0; i < charBoxes.length; i++ ) {
                charTable.removeChild( charBoxes[ i ] );
            }
            doc.removeChild( charTable );

            if ( this.parent ) {
                this.parent.removeChild( this );
            }
            trace( '∙ CharTable.clear' );
            callBack();
        }
    }
}
