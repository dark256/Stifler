/**
 * Created by dark256 on 20.06.17.
 */
package {

    import com.greensock.TweenLite;
    import com.greensock.easing.Elastic;

    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.events.Event;

    public class AboutWindow extends MovieClip {

        private var doc:DisplayObjectContainer;
        private var about:aboutWin;
        private var callBack:Function;

        public function AboutWindow( doc:DisplayObjectContainer, callBack:Function ) {
            trace( '∙ AboutWindow' );

            this.doc = doc;
            this.callBack = callBack;

            about = new aboutWin();
            about.addEventListener( Event.ADDED_TO_STAGE, OnAddToStage );
            doc.addChild( about );
        }

        private function OnAddToStage( e:Event ):void {
            about.removeEventListener( Event.ADDED_TO_STAGE, OnAddToStage );
            about.x = about.stage.stageWidth/2;
            TweenLite.to( about, 1, {'y': 30, ease: Elastic.easeOut, onComplete: callBack } );
        }

        public function close():void{
            TweenLite.to( about, 0.3, {'alpha': 0, ease: Elastic.easeOut, onComplete: onCloseComplete } );
        }

        private function onCloseComplete():void{
            doc.removeChild( about );
            if ( this.parent ) {
                this.parent.removeChild( this );
            }
        }
    }
}
